package cn.lhc.test.datasyn.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @ProjectName: datasyn
 * @Package: cn.lhc.test.datasyn.controller
 * @ClassName: TestController
 * @Author: lhc
 * @Description: TODO
 * @Date: 2019/10/21 下午 9:23
 */
@Slf4j
@RestController
@RequestMapping(value = "/test")
public class TestController {
    @RequestMapping(value = "/test")
    public Map<String, Object> test(@RequestBody Map<String, Object> map) {
        Map<String, Object> result = new HashMap<>(2);
        result.put("code", 200);
        result.put("msg", "SUCCESS");
        log.info("request:{}", map.toString());
        return result;
    }
}
