package cn.lhc.test.datasyn;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.lhc.test.datasyn.dao")
public class DatasynApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatasynApplication.class, args);
    }

}
