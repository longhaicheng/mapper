package cn.lhc.test.datasyn.dao;

import cn.lhc.test.datasyn.model.Customer;
import tk.mybatis.mapper.common.Mapper;

public interface CustomerMapper extends Mapper<Customer> {
}