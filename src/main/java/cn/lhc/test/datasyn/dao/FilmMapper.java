package cn.lhc.test.datasyn.dao;

import cn.lhc.test.datasyn.common.BaseDao;
import cn.lhc.test.datasyn.model.Film;

public interface FilmMapper extends BaseDao<Film> {
}