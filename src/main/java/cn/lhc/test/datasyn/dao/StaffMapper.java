package cn.lhc.test.datasyn.dao;

import cn.lhc.test.datasyn.common.BaseDao;
import cn.lhc.test.datasyn.model.Staff;

public interface StaffMapper extends BaseDao<Staff> {
}