package cn.lhc.test.datasyn.common;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @ProjectName: datasyn
 * @Package: cn.sccl.nlp.datasyn.common
 * @ClassName: BaseDao
 * @Author: lhc
 * @Description: TODO
 * @Date: 2019/10/17 下午 5:06
 */
public interface BaseDao<T> extends Mapper<T>, MySqlMapper<T> {
}