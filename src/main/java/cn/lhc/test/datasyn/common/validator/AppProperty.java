package cn.lhc.test.datasyn.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ProjectName: datasyn
 * @Package: cn.sccl.nlp.datasyn.common
 * @ClassName: AppProperty
 * @Author: lhc
 * @Description: TODO
 * @Date: 2019/10/18 上午 10:50
 */
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = AppPropertyConstraintValidator.class)
public @interface AppProperty {

    String message() default "参数不匹配";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
