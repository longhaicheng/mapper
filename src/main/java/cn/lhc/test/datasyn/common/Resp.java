package cn.lhc.test.datasyn.common;

import org.springframework.http.HttpStatus;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @ProjectName: uublog
 * @Package: com.lhc.uublog.common
 * @ClassName: Resp
 * @Author: lhc
 * @Description: 返回基础类
 * @Date: 2019/8/26 下午 11:41
 */
public class Resp extends LinkedHashMap<String, Object> {

    private static final long serialVersionUID = 4941398730825868814L;

    public Resp() {
        put("code", 10200);
        put("message", "SUCCESS");
    }

    public static Resp ok() {
        return new Resp();
    }

    public static Resp ok(String message) {
        Resp resp = new Resp();
        resp.put("message", message);
        return resp;
    }

    public static Resp ok(Map<String, Object> map) {
        Resp resp = new Resp();
        resp.putAll(map);
        return resp;
    }

    public static Resp error(int code, String message) {
        Resp resp = new Resp();
        resp.put("code", code);
        resp.put("message", message);
        return resp;
    }

    public static Resp error() {
        return error(HttpStatus.INTERNAL_SERVER_ERROR.value(), "ERROR");
    }

    public static Resp error(String message) {
        return error(10500, message);
    }

    @Override
    public Object put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}

