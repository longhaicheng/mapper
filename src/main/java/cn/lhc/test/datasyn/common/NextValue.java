package cn.lhc.test.datasyn.common;

/**
 * @author think
 */
public class NextValue {
    private static SnowflakeIdWorker snowflakeIdWorker;

    public NextValue() {
        snowflakeIdWorker = new SnowflakeIdWorker();

    }

    public long getNextBig() {
        long nextId = snowflakeIdWorker.nextId();
        return nextId;
    }

}
