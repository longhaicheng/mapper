package cn.lhc.test.datasyn.common.validator;

/**
 * @ProjectName: datasyn
 * @Package: cn.sccl.nlp.datasyn.common
 * @ClassName: ConstraintNotMatchException
 * @Author: lhc
 * @Description: TODO
 * @Date: 2019/10/18 上午 10:58
 */
public class ConstraintNotMatchException extends RuntimeException {
    private static final long serialVersionUID = -6213954745418787581L;

    public ConstraintNotMatchException() {
    }

    public ConstraintNotMatchException(String message) {
        super(message);
    }
}
