package cn.lhc.test.datasyn.common.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ProjectName: acp
 * @ClassName: VoiceLog
 * @Author: lhc
 * @Description: 自定义注解
 * @Date: 2019/7/31 上午 10:25
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceLog {
}
