package cn.lhc.test.datasyn.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @ProjectName: datasyn
 * @Package: cn.sccl.nlp.datasyn.common
 * @ClassName: AppPropertyConstraintValidator
 * @Author: lhc
 * @Description: 自定义验证
 * @Date: 2019/10/18 上午 10:53
 */
public class AppPropertyConstraintValidator implements ConstraintValidator<AppProperty, Object> {
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Integer appProperty = (Integer) value;
        System.out.println(Math.abs(appProperty) < 2);
        return Math.abs(appProperty) < 2;
    }
}
