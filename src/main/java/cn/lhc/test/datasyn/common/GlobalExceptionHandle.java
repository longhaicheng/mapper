package cn.lhc.test.datasyn.common;

import cn.lhc.test.datasyn.common.validator.ConstraintNotMatchException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ProjectName: uublog
 * @Package: com.lhc.uublog.common
 * @ClassName: GlobalExceptionHandle
 * @Author: lhc
 * @Description: TODO
 * @Date: 2019/8/29 下午 1:46
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandle {


    @ResponseBody
    @ExceptionHandler(ConstraintNotMatchException.class)
    public Resp constraintNotMatchException(ConstraintNotMatchException e) {
        return Resp.error(10400, e.getMessage());
    }
}
