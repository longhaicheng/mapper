package cn.lhc.test.datasyn.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Staff extends AbstractModel implements Serializable {
    @Id
    @Column(name = "staff_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Byte staffId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "address_id")
    private Short addressId;

    private String email;

    @Column(name = "store_id")
    private Byte storeId;

    private Boolean active;

    private String username;

    private String password;

    @Column(name = "last_update")
    private Date lastUpdate;

    private byte[] picture;

    private static final long serialVersionUID = 1L;
}