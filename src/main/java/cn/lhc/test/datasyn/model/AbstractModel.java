package cn.lhc.test.datasyn.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @ProjectName: datasyn
 * @Package: cn.sccl.nlp.datasyn.model
 * @ClassName: AbstractModel
 * @Author: lhc
 * @Description: TODO
 * @Date: 2019/10/17 下午 4:47
 */
public abstract class AbstractModel {
    @Override
    public String toString() {
        Gson gson = new GsonBuilder()
                .serializeNulls()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
        return gson.toJson(this);
    }
}
