自己写的一个脚手架，后面可以直接拿来用
### 功能：
* Hibernate-Validator参数校验
* AOP日志拦截
    * 注解式
    * 表达式
 * 返回封装类`Resp`
 * [通用Mapper插件](https://mapperhelper.github.io/docs/)，实现单表无SQL
 * Mybatis Generator和Mapper插件的整合，快速生成dao和model
 * 基于雪花算法的工具类`NextValue`
